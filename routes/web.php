<?php

Route::get('/', function () {
    return view('welcome');
});

Route::get('columns', 'ColumnsController@getBoardData');
Route::patch('/column/{id}/capacity/{capacity}','ColumnsController@setColumnCapacity');

Route::post('cards', 'CardsController@createNewCard');
Route::delete('cards/{id}', 'CardsController@delete');
Route::patch('/card/{id}/status/{status}', "CardsController@updateCardStatus");
Route::patch('/cards/order/','CardsController@setCardsOrder');




