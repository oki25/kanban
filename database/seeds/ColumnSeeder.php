<?php

use Illuminate\Database\Seeder;
use App\Column;

class ColumnSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Column::create(['name'=>'To Do']);
        Column::create(['name'=>'Doing', 'capacity' => 3]);
        Column::create(['name'=>'Done']);    
    }
}
