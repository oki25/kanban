<?php

use Illuminate\Database\Seeder;
use App\Card;

class CardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Card::create(['title'=>'Task1','description'=>'Description','status'=>1,'position'=>1]);
        Card::create(['title'=>'Task2','description'=>'Description','status'=>1,'position'=>2]);
        Card::create(['title'=>'Task3','description'=>'Description','status'=>1,'position'=>3]);
        Card::create(['title'=>'Task4','description'=>'Description','status'=>2,'position'=>3]);
        Card::create(['title'=>'Task5','description'=>'Description','status'=>3,'position'=>3]);
    }
}
