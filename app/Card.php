<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Column;

class Card extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = ['title', 'description', 'status'];
    
    public function column()
    {
        return $this->belongsTo(Column::class);
    }
}
