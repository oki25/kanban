<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Card;

class Column extends Model
{
    public function cards()
    {
        return $this->hasMany(Card::class);
    }
}
