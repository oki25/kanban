<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Column;
use App\Services\ColumnService;

class ColumnsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     /**
     * @var ColumnService
     */
    private $columnService;

    public function __construct(ColumnService $columnService)
    {
        $this->columnService = $columnService;
    }

    //Fetching all data that is needed for board, getting cards in the right order
    public function getBoardData()
    {
        return $this->columnService->getColumnsWithCards();
    }

    public function setColumnCapacity($id, $capacity)
    {
        return $this->columnService->setCapacity($id, $capacity);
    }
  
}
