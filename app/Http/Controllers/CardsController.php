<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Card;
use App\Services\CardService;
 
class CardsController extends Controller
{

    private $cardService;

    public function __construct(CardService $cardService)
    {
        $this->cardService = $cardService;
    }

    public function index()
    {
        return $this->cardService->getCards();
    }

    public function createNewCard(Request $request)
    {
        return $this->cardService->createNewCard($request);
    }

    public function show($id)
    {
        return $this->cardService->getCardById($id);
    }

    public function updateCardStatus($id, $status)
    {
        return $this->cardService->updateCardStatus($id, $status);
    }

    public function setCardsOrder(Request $request)
    {
        return $this->cardService->setOrder($request);
    }

    public function delete($id)
    {
        return $this->cardService->deleteCard($id);
    }
}
