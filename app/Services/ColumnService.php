<?php

namespace App\Services;
use App\Column;
use App\Card;

class ColumnService
{
    public function getColumnsWithCards()
    {
        $columns = Column::all();
        foreach ($columns as $column) {
            $cards = Card::where('status', $column->id)->orderBy('position')->get();
            $column->cards=$cards;
        }
        return response()->json(['columns' => $columns], 200);
    }
    
    public function setCapacity($id, $capacity)
    {
        $column = Column::find($id);
        $column->capacity = $capacity;
        $column->save();
        return response()->json($column, 200);
    }
}