<?php

namespace App\Services;
use App\Column;
use App\Card;

class CardService
{
   
    public function setCapacity($id, $capacity)
    {
        $column = Column::find($id);
        $column->capacity = $capacity;
        $column->save();
        return response()->json($column, 200);
    }

    public function createNewCard($request)
    {
        if($this->capacityCheck($request->status)){
            return response()->json(403);
        }
       
        /*
            Setting card position value same as card id so every card
            would have unique value of position that helps us sort them in
            desired order
        */
        $card = Card::create([
            'title'=>$request->title, 
            'description'=>$request->description,
            'status'=>$request->status
            ]);
        $card->position = $card->id;
        $card->save();
       
        return response()->json(['card' => $card], 201);
    }

    public function updateCardStatus($id, $status)
    {
        if($this->capacityCheck($status)){
            return response()->json(403);
        }
        $card = Card::find($id);
        $card->status = $status;
        $card->save();
        return response()->json($card, 200);
    }

    public function setOrder($request)
    {
        $obj = $request->all(); 
        foreach($obj as $a){
            $obj = (object)$a;
            $card = Card::find($obj->id);
            $card->position = $obj->position;
            $card->save();
        }
        return response()->json(200); 
    }

    public function deleteCard($id)
    {
        $card = Card::find($id);
        if($card === null){
            return response()->json(404);
        }
        $card->delete();
        return response()->json($card, 200);
    }
    
    /*  
        Checking if column has reached it's maximum capacity, so that
        we will have backend app protection as well as front end
    */
    private function capacityCheck($id){
        $column = Column::find($id);
        $cards = Card::where('status', $column->id)->get();
        
        if($column->capacity !== null && count($cards) >= $column->capacity){
            return true;
        }else{
            return false;
        }
    }

    
   


}