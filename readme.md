# Kanban Board

-   Kanban board has columns. Each column is named, and optionally user can set column's max capacity
-   Cards are placed in columns and user can add and delete cards
-   User can drag and drop them into a new position in the same column, or to another column
-   When user sets column's capacity, that column can't have more the given number of cards in it
-   Cards can't be moved to a column that reached it's capacity neither can cards be created in it

## Requirements

-   Web server:
    -   [XAMPP](https://www.apachefriends.org/index.html) - Windows
    -   [LAMP](https://bitnami.com/stack/lamp/installer) - Linux
-   [Composer](https://getcomposer.org/download/) - Dependency Manager for PHP

## Installation

-   Clone the repository
-   Create database:
    -   Open command line and navigate to MySql bin folder
    -   run: `mysqladmin -u root -p create kanban`
    -   Note: Make sure that your MySql server is running
-   Install dependencies (from console):
    -   Navigate to laravel project
    -   run `composer install`
-   Modify .env.example file:
    -   Rename the file to .env
    -   In .env file set your database credentials
-   Generate application key:
    -   run `php artisan key:generate`
-   Run migrations: `php artisan migrate:refresh --seed`
-   For more detailed information, see Laravel documentation:
    -   [Laravel](https://laravel.com/) PHP Framework

## Running

```
php artisan serve
```

Open browser on `http://localhost:8000`

## Enjoy!
